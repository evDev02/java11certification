package packages.package2;

public class OtherPackage {
    OtherPackage(){
        packages.package1.Protection p1 = new packages.package1.Protection();
        System.out.println("other package constructor");

        //class or package only
        //System.out.println("n = " + p1.n);

        //class only
        //System.out.println("n_pri = " + p1.n_pri);

        //class, subclass or package only
        //System.out.println("n_pro = " + p1.n_pro);
        System.out.println("n_pub = " + p1.n_pub);
    }
}
