package AbstractClasses;

abstract public class ClassA {
    abstract void callme();

    void callmetoo(){
        System.out.println("This is a concrete method");
    }
}
