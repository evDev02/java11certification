package AbstractClasses;

public class AbstractDemo {
    public static void main(String args[]){
        ClassB b = new ClassB();

        b.callme();
        b.callmetoo();
    }
}
