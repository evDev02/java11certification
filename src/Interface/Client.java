package Interface;

class Client implements Callback {
    public void callback(int p){
        System.out.println("callback called with: " + p);
    }
    public void otherMethod(){
        System.out.println("other method to print");
    }
}
