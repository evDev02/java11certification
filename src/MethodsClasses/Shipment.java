package MethodsClasses;

public class Shipment extends BoxWeight {
    double cost;

    //constructor clone of an object
    Shipment(Shipment ob){
        super(ob);
        cost = ob.cost;
    }

    //constructor when all paramenters are defined
    Shipment(double w, double h, double d, double m, double c){
        super(w, h, d, m);
        cost = c;
    }

    //default constructor
    Shipment(){
        super();
        cost = -1;
    }

    //constructor used when cube is created
    Shipment(double len, double m, double c){
        super(len, m);
        cost = c;
    }

    void show(){
        super.show();
        System.out.println("Imprimir valores en Shipment: cost = " + cost + ", height = " + height);
    }
}
