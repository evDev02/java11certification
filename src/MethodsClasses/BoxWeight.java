package MethodsClasses;

public class BoxWeight extends Box {
    double weight;

    //constructor for weightBox
    BoxWeight(double w, double h, double d, double m){
        super(w,h,d);
//        width = w;
//        height = h;
//        depth = d;
        weight = m;
    }

    BoxWeight(BoxWeight ob){
        super(ob);
        weight = ob.weight;
    }

    BoxWeight(){
        super();
        weight = -1;
    }

    BoxWeight(double len, double m){
        super(len);
        weight = m;
    }

    void show(){
        System.out.println("Imprimir valores en BoxWeight: weight = " + weight + ", height = " + height);
    }
}
